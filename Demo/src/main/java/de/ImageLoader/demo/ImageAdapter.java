package de.ImageLoader.demo;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import de.ImageLoader.demo.R;
import de.bitninja.ImageLoader.Imageloader;

import java.util.ArrayList;

/**
 * Created by Kaizer on 20.05.13.
 */
public class ImageAdapter extends PagerAdapter {
    Context context;
    ArrayList<String> _list;
    Imageloader iLoader;

    ImageAdapter(Context context, ArrayList<String> list){
        this._list = list;
        this.context=context;
        this.iLoader = new Imageloader(context);
    }
    @Override
    public int getCount() {
        return _list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((ImageView) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imageView = new ImageView(context);
        int padding = 10;
        imageView.setPadding(padding, padding, padding, padding);
        imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        iLoader.load(_list.get(position),imageView);
        //imageView.setImageResource(GalImages[position]);
        ((ViewPager) container).addView(imageView, 0);
        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((ImageView) object);
    }
}