package de.bitninja.ImageLoader;

/**
 * Created with IntelliJ IDEA.
 * User: dgrigutsch
 * Date: 04.04.13
 * Time: 09:03
 * To change this template use File | Settings | File Templates.
 */

import java.io.IOException;
import java.io.OutputStream;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.v4.util.LruCache;
import android.util.Log;

public class ImageCache  {
	
	private static final int DEFAULT_MEM_CACHE_SIZE = 1024 * 1024 * 4; // 4MB
	private static final int DEFAULT_DISK_CACHE_SIZE = 1024 * 1024 * 20; // 20MB
	
	private static LruCache<String, Bitmap> cache = null;
	private static DiskLruCache diskCache = null;
	
	private Context ctx;
	

	
	@SuppressLint("NewApi")
	public ImageCache(Context context) {
        this.ctx = context;
        
        // Use a WeakReference to ensure the ImageView can be garbage collected
       if (cache == null) {
	       // final int memClass = ((ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE)).getMemoryClass();
		       // cache = new LruCache<String, Bitmap>(1024 * 1024 * memClass / 3) {
        		cache = new LruCache<String, Bitmap>(DEFAULT_MEM_CACHE_SIZE) {
		        @Override
		        protected int sizeOf(String key, Bitmap bitmap) {
	            	return (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1)
	                        ? bitmap.getByteCount()
	                        : bitmap.getRowBytes() * bitmap.getHeight();
	            }
	        };
        }
        
       if (diskCache == null) {
        	try {
        	diskCache = DiskLruCache.open(ctx.getCacheDir(), 1, 1, DEFAULT_DISK_CACHE_SIZE);
        	} catch (IOException e) {
        		e.printStackTrace();
        	}
       }
	}
	
	public boolean containsBitmap(String key){
		boolean contained = false;
		try{
			if(null != cache.get(key)){
	        	contained = true;
	        }
			else if(null != diskCache.get(key)){
	        	contained = true;
	        }
		} catch(Exception e){
			e.printStackTrace();
		}
        return contained;
	}
       
	
    public int getDiskSize(){
    	return (int) diskCache.size();
    }
    
    public int getMemSize(){
    	return (int) cache.size();
    }
       
    public Bitmap getBitmapFromCache(String key) {
    	Bitmap bitmap = getBitmapFromMemCache(key);
    	if (bitmap == null) {
    		   bitmap = getBitmapFromDiskCache(key);
	    	if (bitmap != null) {
	    		   addBitmapToCache(key, bitmap);
	    	}
    	}
    	return bitmap;
    }
    	    
      public void addBitmapToCache(String key, Bitmap bitmap) {
          if ( null == getBitmapFromMemCache(key)) {
    		   addBitmapToMemoryCache(key, bitmap);
    	   }
    	   if (null == getBitmapFromDiskCache(key)) {
    		   addBitmapToDiskCache(bitmap, key);
    	   }
      }
      
      private void addBitmapToMemoryCache(String key, Bitmap bitmap) {
    	  if (getBitmapFromMemCache(key) == null) {
    		  cache.put(key, bitmap);
    	  }
      }
 
      private void addBitmapToDiskCache(Bitmap bitmap, String key) {
    	 try {
	    	 DiskLruCache.Editor editor = diskCache.edit(key.hashCode() + "");
	    	 if (editor != null) {
		    	 OutputStream os = editor.newOutputStream(0);
		    	 bitmap.compress(Bitmap.CompressFormat.PNG, 100, os);
		    	 editor.commit();
	    	 }
    	 } catch (IOException e) {
    		 e.printStackTrace();
    	 }
      }
     
      private Bitmap getBitmapFromMemCache(String key) {
    	 return cache.get(key);
      }
    
      private Bitmap getBitmapFromDiskCache(String key) {
    	Bitmap bitmap = null;
    	try {
	    	DiskLruCache.Snapshot snapshot = diskCache.get(key.hashCode() + "");
	    	if (snapshot != null) {
	    		bitmap = BitmapFactory.decodeStream(snapshot.getInputStream(0));
	    	}
    	} catch (IOException e) {
    		bitmap = null;
    	}
    	return bitmap;
    }
	
}