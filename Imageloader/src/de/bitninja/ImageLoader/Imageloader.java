package de.bitninja.ImageLoader;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.widget.ImageView;

import java.io.*;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created with IntelliJ IDEA.
 * User: dgrigutsch
 * Date: 04.04.13
 * Time: 09:03
 * To change this template use File | Settings | File Templates.
 */
public class Imageloader {

    private ExecutorService executorService;
    private int placeholder = -1;
    private Map<ImageView, String> imageViews = Collections.synchronizedMap(new WeakHashMap<ImageView, String>());
    //private DiskLruImageCache cache;
    private ImageCache fileCache;
    private boolean refresh = false;

    public Imageloader(Context context) {
        executorService = Executors.newFixedThreadPool(5);
        //cache = new DiskLruImageCache(context);
        fileCache = new ImageCache(context);
    }

    //##

    public void load(String url, ImageView imageView) {
        load(url, imageView, -1,-1,-1,false);
    }
    public void load(String url, ImageView imageView, boolean refresh) {
        load(url, imageView, -1,-1,-1,refresh);
    }
     
    public void load(String url, ImageView imageView, int placeholder) {
    	load(url, imageView, placeholder,-1,-1,false);
    }
    
    public void load(String url, ImageView imageView, int placeholder, boolean refresh) {
    	load(url, imageView, placeholder,-1,-1,refresh);
    }
    
    public void load(String url, ImageView imageView, int placeHolder, int width, int height) {
    	load(url, imageView, placeholder,width,height,false);
    }

    public void load(String url, ImageView imageView, int placeHolder, int width, int height, boolean _refresh) {
    	try {
    		//if(cache.containsKey(tag)){
    		this.refresh = _refresh;
            //String tag = Integer.valueOf(url.substring(0,url.indexOf(";")).hashCode()).toString();
            String tag = Integer.valueOf(url.hashCode()).toString();
            imageViews.put(imageView, tag);           
            if(fileCache.containsBitmap(tag) && refresh == false){
                imageView.setImageBitmap(fileCache.getBitmapFromCache(tag));
            } else {
	            if(placeholder != -1){
	    	        this.placeholder = placeHolder;
	    	        imageView.setImageResource(placeholder);
	            }
	            //imageView.setImageResource(null);
               // imageView.setImageDrawable(new ColorDrawable(Color.parseColor("#FFFFFF")));
            	executorService.submit(new BitmapDownloaderTask(imageView,url,width,height));
            }	
    	} catch ( Exception e) {
    		e.printStackTrace();
    	}
        
    }


    class BitmapDownloaderTask implements Runnable {
        private String url;
        private int width = -1;
        private int height = -1;
        private final SoftReference<ImageView> imageViewReference;

        public BitmapDownloaderTask(ImageView imageView, String Url){
            this.imageViewReference = new SoftReference<ImageView>(imageView);
            this.url = Url;
        }

        public BitmapDownloaderTask(ImageView imageView, String Url, int width, int height){
            this.imageViewReference = new SoftReference<ImageView>(imageView);
            this.url = Url;
            this.width = width;
            this.height = height;
        }

        @Override
        public void run() {
            try {
                if(imageViewReused(this))
                    return;
                Activity a = (Activity) imageViewReference.get().getContext();
                Bitmap bmp = getImage(url, width, height);
                a.runOnUiThread(new BitmapDisplayer(bmp,this));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private Bitmap getImage(String Url, int width, int height){
            try {
                Bitmap bmp = null;
                String tag = Integer.valueOf(url.hashCode()).toString();

                final BitmapFactory.Options options = new BitmapFactory.Options();
                if(width != -1 && height != -1)  {
                    options.inJustDecodeBounds = true;
                    options.inSampleSize = Utils.calculateInSampleSize(options, width, height);
                    options.inJustDecodeBounds = false;
                }

                //File f = fileCache.getBitmap(tag);
                if(refresh == false){
	                bmp = fileCache.getBitmapFromCache(tag);
	                //bmp = decodeFile(f,options);
                }
                if(null != bmp){
                    return bmp;
                }
                Log.e("URL",url);
                BufferedInputStream is = new BufferedInputStream(new SSLClient().getResponseFromUrl(url).getEntity().getContent());
                //OutputStream os = new FileOutputStream(f);
                //Utils.CopyStream(is, os);
                //os.close();

                //bmp = decodeFile(f,options);
                bmp = BitmapFactory.decodeStream(is);
                Log.e("TAG",tag);
                fileCache.addBitmapToCache(tag, bmp);
                return bmp;

            }catch (Exception e){
               e.printStackTrace();
            }
            return null;
        }
    }

        boolean imageViewReused(BitmapDownloaderTask task) {
            String Tag = imageViews.get(task.imageViewReference.get());
            if (Tag == null || !Tag.equals(task.url))
                return false;
            return true;
        }
       //+++
    // Used to display bitmap in the UI thread
    class BitmapDisplayer implements Runnable {
        Bitmap bitmap;
        BitmapDownloaderTask photoToLoad;

        public BitmapDisplayer(Bitmap b, BitmapDownloaderTask p) {
            bitmap = b;
            photoToLoad = p;
        }

        public void run() {
            if (imageViewReused(photoToLoad))
                return;

            if ( null != bitmap) {
                photoToLoad.imageViewReference.get().setImageBitmap(bitmap);
            } /*else {
                 photoToLoad.imageViewReference.get().setImageResource(placeholder);
            }*/
        }
    }

    private Bitmap decodeFile(File f, BitmapFactory.Options options) {
        try {
            BitmapFactory.Options o2 = options;
            // o2.inSampleSize = scale;
            FileInputStream stream2 = new FileInputStream(f);
            Bitmap bitmap = BitmapFactory.decodeStream(stream2, null, o2);
            stream2.close();
            return bitmap;

        } catch (FileNotFoundException e) {
        	e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}


